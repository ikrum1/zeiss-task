"use strict";

const request    = require("supertest");
const httpStatus = require("http-status");
const chai       = require("chai");
const app        = require("../src/app");
const Store      = require("../src/store/store");
const logStore   = require("../src/store/logStore");
const expect     = chai.expect;

const sampleLog = [
  {
    "timestamp": "2019-02-16T12:20:54.999569Z",
    "status": "running",
    "machine_id": "759c98a4-b66f-4799-b278-bab8aec881a0",
    "id": "f0d0ecdd-b79d-4df8-8d53-9ee0f4dae126"
  },
  {
    "timestamp": "2019-02-16T12:21:00.079115Z",
    "status": "finished",
    "machine_id": "211bd1c5-9230-4ca3-8cc8-9c3226646b99",
    "id": "09324c3d-151a-42c4-b813-5678b67ba566"
  },
  {
    "timestamp": "2019-02-16T12:21:25.199882Z",
    "status": "running",
    "machine_id": "759c98a4-b66f-4799-b278-bab8aec881a0",
    "id": "c98aa025-2ff0-4e07-af65-84b76fecf00b"
  }
];



describe("## Assset monitor logs", () => {
  sampleLog.forEach(item => logStore.insert(item));

  describe("# Store Class", () => {
    let testStore;
    it("should init store and insert new logs to the storage", (done) => {
      testStore = new Store();
      sampleLog.forEach(item => testStore.insert(item));

      expect(sampleLog.length).to.equal(testStore.getAll().length);
      done();
    });

    it("should filter running machines", (done) => {
      expect(testStore.find({status: "running"}).length).to.equal(2);
      done();
    });
  });

  describe("# API: GET /api/assets/logs", () => {
    it("should fetch all the asset logs", (done) => {
      request(app)
        .get("/api/assets/logs")
        .expect("Content-Type", /json/)
        .expect(httpStatus.OK)
        .end(function(err, res) {
          expect(res.body.data.length).to.be.equal(sampleLog.length);
          if (err) return done(err);
          done();
        });
    });

    it("should filter logs by status", (done) => {
      request(app)
        .get("/api/assets/logs")
        .query({status: "running"})
        .expect("Content-Type", /json/)
        .expect(httpStatus.OK)
        .end(function(err, res) {
          expect(res.body.data.length).to.be.equal(2);
          if (err) return done(err);
          done();
        });
    });

    it("should fetch logs of a single machine_id", (done) => {
      request(app)
        .get("/api/assets/logs/759c98a4-b66f-4799-b278-bab8aec881a0")
        .query({status: "running"})
        .expect("Content-Type", /json/)
        .expect(httpStatus.OK)
        .end(function(err, res) {
          expect(res.body.data.length).to.be.equal(2);
          if (err) return done(err);
          done();
        });
    });
  });

  describe("# API: GET /api/assets/status", () => {
    it("should fetch the last status of a particular machine", (done) => {
      request(app)
        .get("/api/assets/status/211bd1c5-9230-4ca3-8cc8-9c3226646b99")
        .expect("Content-Type", /json/)
        .expect(httpStatus.OK)
        .end(function(err, res) {
          expect(res.body.data.status).to.be.equal("finished");
          if (err) return done(err);
          done();
        });
    });
  });
});
