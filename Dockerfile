FROM node:10.15-alpine

ADD package.json package.json
RUN npm install
ADD . /application
WORKDIR /application

EXPOSE  8080
CMD ["npm","run", "production"]