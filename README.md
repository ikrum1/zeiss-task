# ZEISS task
## Requirements
NodeJS with ES6 Support and make sure port 8080 is free. Otherwise, please change the port before running

### Running
Running on docker
```
docker-compose up --build
```

Running test
```
npm test
```

Run in dev environment
```
# Includes lint checks
npm start
```

Generating Doc
```
npm run docs
```


# APIs
### Get ALL Logs
returns all the logs received from the ws://machinestream.herokuapp.com/ws

```
GET localhost:8080/api/assets/logs

{
    "error": false,
    "data": [
        {
            "timestamp": "2019-02-16T14:14:00.798915Z",
            "status": "running",
            "machine_id": "799819f8-6c19-47cc-9e3f-b9438b3bed4f",
            "id": "12495fbf-89e6-4af2-b1ba-82fa6968b5fc"
        }
    ]
}
```

You can filter by `status` and `machine_id`

```
GET /api/assets/logs?status=finished&machine_id=cf0959dd-39ba-4c56-90a4-582c2d7a9482

{
    "error": false,
    "data": [
        {
            "timestamp": "2019-02-16T14:14:36.035084Z",
            "status": "finished",
            "machine_id": "cf0959dd-39ba-4c56-90a4-582c2d7a9482",
            "id": "1ecc41fa-ccf5-4c36-8355-f9e5c9ef06b8"
        }
    ]
}
```

### Get logs of a particular machine
```
GET api/assets/logs/cf0959dd-39ba-4c56-90a4-582c2d7a9482

{
    "error": false,
    "data": [
        {
            "timestamp": "2019-02-16T14:14:36.035084Z",
            "status": "finished",
            "machine_id": "cf0959dd-39ba-4c56-90a4-582c2d7a9482",
            "id": "1ecc41fa-ccf5-4c36-8355-f9e5c9ef06b8"
        }
    ]
}
```

### Get status of a machine
Return the last/latest log item

```
GET /api/assets/status/:machine_id
GET /api/assets/status/cf0959dd-39ba-4c56-90a4-582c2d7a9482

{
    "error": false,
    "data": {
        "timestamp": "2019-02-16T14:14:36.035084Z",
        "status": "finished",
        "machine_id": "cf0959dd-39ba-4c56-90a4-582c2d7a9482",
        "id": "1ecc41fa-ccf5-4c36-8355-f9e5c9ef06b8"
    }
}
```

# Screenshots
### Test Result
![](screenshot_test_result.png)

### Data
![](screenshot_data.png)