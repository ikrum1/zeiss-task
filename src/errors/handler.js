"use strict";

const { ApiError, InternalServerError, BadRequestError } = require("./apiErrors");

/**
 * Wrap route function for errors
 * @param  {function} func controller function to be listened for errors
 * @return {void}
 */
function wrap(func) {
  return async function (req, res, next) {
    try {
      await func(req, res, next);
    } catch (err) {
      next(err);
    }
  };
}


/**
 * Parse error to ApiError
 * @param  {Mixed} err the error
 * @return {ApiError} parsed object
 */
function sanitizeError(err) {
  if (err instanceof ApiError) return err;
  if (typeof err === "string") return new BadRequestError(err);

  if(Array.isArray(err)){
    return new BadRequestError(err[0].message.replace(/"/g, "'"));
  }

  return new InternalServerError();
}

module.exports = { wrap, sanitizeError };
