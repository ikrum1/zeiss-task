"use strict";

const httpStatus = require("http-status");

/**
 * @module Errors
 * @summary all errors here
 */

/**
 * @memberof module:Errors
 * @summary class ApiError will be used to parse custom api errors accross the system
 */


class ApiError {
  /**
     * @constructor
     *
     * @param  {string} name    Name of the error
     * @param  {string} message Message of the error
     * @param  {Number} status  Http staus code
     * @return {void}
     */
  constructor(name = "ApiError", message, status = httpStatus.INTERNAL_SERVER_ERROR) {
    this.message = message;
    this.status = status;
  }
  
  /**
     * @static
     * @return {object} Response body of an error
     */
  get response() {
    return { error: true, message: this.message };
  }
}
  
/**
   * @memberof module:Errors
   * @extends ApiError
   */
class InternalServerError extends ApiError {
  /**
     * @constructor
     * @param  {String} [message='Internal Server Error']     message for the bad request
     * @return {void}
     */
  constructor(message = "Internal Server Error") {
    super("InternalServerError", message, httpStatus.INTERNAL_SERVER_ERROR);
  }
}
  
/**
   * @memberof module:Errors
   * @extends ApiError
   */
class BadRequestError extends ApiError {
  /**
     * @constructor
     * @param  {String} [message='Invalid request']     message for the bad request
     * @return {void}
     */
  constructor(message = "Invalid request") {
    super("BadRequestError", message, httpStatus.BAD_REQUEST);
  }
}
/**
   * @memberof module:Errors
   * @extends ApiError
   */
class MissingParamsError extends ApiError {
  /**
     * @constructor
     * @param  {String} [message='Missing Required parameter(s)'] message for the missing params
     * @return {void}
     */
  constructor(message = "Missing Required parameter(s)") {
    super("MissingParamsError", message, httpStatus.BAD_REQUEST);
  }
}
  
/**
   * @memberof module:Errors
   * @extends ApiError
   */
class InvalidTokenError extends ApiError {
  /**
     * @constructor
     * @param  {String} [message='Authorization token is invalid'] message for the InvalidTokenError
     * @return {void}
     */
  constructor(message = "Authorization token is invalid") {
    super("InvalidTokenError", message, httpStatus.UNAUTHORIZED);
  }
}
/**
   * @memberof module:Errors
   * @extends ApiError
   */
class InvalidAccessError extends ApiError {
  /**
     * @constructor
     * @param  {String} [message='Access denied']      message for InvalidAccessError
     * @return {void}
     */
  constructor(message = "Access denied") {
    super("InvalidAccessError", message, httpStatus.FORBIDDEN);
  }
}
/**
   * @memberof module:Errors
   * @extends ApiError
   */
class PageNotFoundError extends ApiError {
  /**
     * @constructor
     * @param  {String} [message='Record not found'] message for RecordNotFoundError
     * @return {void}
     */
  constructor(message = "Page not found") {
    super("PageNotFoundError", message, httpStatus.NOT_FOUND);
  }
}
/**
   * @memberof module:Errors
   * @extends ApiError
   */
class RecordNotFoundError extends ApiError {
  /**
     * @constructor
     * @param  {String} [message='Record not found'] message for RecordNotFoundError
     * @return {void}
     */
  constructor(message = "Record not found") {
    super("RecordNotFoundError", message, httpStatus.NOT_FOUND);
  }
}
  
/**
   * @memberof module:Errors
   * @extends ApiError
   */
class NotAcceptableError extends ApiError {
  /**
     * @constructor
     * @param  {String} [message='Record not found'] message for RecordNotFoundError
     * @return {void}
     */
  constructor(message = "Not Acceptable") {
    super("NotAcceptableError", message, httpStatus.NOT_ACCEPTABLE);
  }
}
  
module.exports = {
  ApiError,
  InternalServerError,
  BadRequestError,
  MissingParamsError,
  PageNotFoundError,
  RecordNotFoundError,
  InvalidTokenError,
  InvalidAccessError,
  NotAcceptableError,
};
  