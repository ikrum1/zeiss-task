"use strict";

/**
 * @module Store
 * @summary store the machine logs
 */

class Store {
  constructor(){
    this.data = [];
  }
  getAll(){
    return this.data;
  }
  find(opts = {}){
    return this.data.filter(item => {
      const keys = Object.keys(opts);
      for(let key of keys){
        if(item[key] != opts[key]) return false;
      }
      return true;
    });
  }
  insert(opts){
    this.data.push(opts);
  } 
}

module.exports = Store;