"use strict";

const express    = require("express");
const helmet     = require("helmet");
const cors       = require("cors");
const logger     = require("morgan");

const config     = require("./config");

const app         = express();
const indexRouter = require("./routes/index");
const {PageNotFoundError} = require("./errors/apiErrors");
const {sanitizeError} = require("./errors/handler");
const winstonInstance = require("./logging/winston");
const socket = require("./socket/socket");


app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(helmet());
app.use(cors());

(config.ENV === "development") && app.use(logger("dev"));
(config.ENV !== "test") && socket.init();

app.use("/", indexRouter);


// catch 404 and forward to error handler
app.use((req, res, next) => next(new PageNotFoundError()));

// error handler, send stacktrace only during development
app.use((err, req, res, next) => {
  const logStatus = [401,403,500];
  const apiError = sanitizeError(err);

  if(logStatus.indexOf(apiError.status) != -1){
    winstonInstance.error(`${apiError.status} - ${err.message} - ${req.method} ${req.originalUrl} - ${JSON.stringify(err.stack)}`);
  }
  
  res.status(apiError.status).json(apiError.response);
});

module.exports = app;
