"use strict";

const WebSocket = require("ws");
const logStore = require("../store/logStore");
const config = require("../config");

function connect() {
  let ws = new WebSocket(config.ZEISS_MONITOR_URL);
  
  ws.on("open", () => {
    console.log("SOCKET: connection opened");
  });

  ws.on("message", data => {
    console.log("SOCKET: data received"); // just to see the activity, not recommended for prod
    logStore.insert(JSON.parse(data).payload);
  });

  ws.on("close", () => {
    console.error("SOCKET connection closed. Reconnecting");
    setTimeout(connect, 0);
  });

  ws.on("error", reason => console.error("SOCKET ERROR: " + reason.toString()));
  return ws;
}


module.exports = { init: () => connect() };