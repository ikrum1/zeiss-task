"use strict";

const express   = require("express");
const assetRoute = require("./asset.route");
const router    = express.Router();

router.get("/", (req, res) => res.json({error: false, message: "API homepage"}));
router.use("/api/assets", assetRoute);

module.exports = router;