"use strict";

const express   = require("express");
const assetCtrl = require("../controllers/asset.controller");
const {wrap}    = require("../errors/handler");
const router    = express.Router();

router.get("/logs/", wrap(assetCtrl.getLogs));
router.get("/logs/:assetID", wrap(assetCtrl.getAssetLogs));
router.get("/status/:assetID", wrap(assetCtrl.getAssetStatus));

module.exports = router;