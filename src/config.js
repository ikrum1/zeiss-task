"use strict";

const config = {
  ENV: process.env.NODE_ENV || "development",
  PORT: process.env.PORT || "8080",
  ZEISS_MONITOR_URL: process.env.ZEISS_MONITOR_URL || "ws://machinestream.herokuapp.com/ws"
};

module.exports = config;
