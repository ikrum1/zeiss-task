"use strict";

const logStore = require("../store/logStore");

exports.getAssetStatus = (req, res) => {
  const assetLogs = logStore.find({machine_id: req.params.assetID});
  res.json({ error: false, data: assetLogs.pop() });
};

exports.getAssetLogs = (req, res) => {
  const assetLogs = logStore.find({machine_id: req.params.assetID});
  res.json({ error: false, data: assetLogs });
};

exports.getLogs = (req, res) => {
  const filterFields = ["machine_id", "status"];
  const filterOpts = {};
  filterFields.forEach(name => {
    if(req.query[name]) filterOpts[name] = req.query[name];
  });

  const allLogs = logStore.find(filterOpts);
  res.json({ error: false, data: allLogs });
};