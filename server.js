"use strict";

const app = require("./src/app");
const port = process.env.PORT || "8080";
const server = app.listen(port);
const winstonInstance = require("./src/logging/winston");

server.on("listening", () => winstonInstance.info(`Listening on ${port}`));
